;;;; Copyright (C) 2020  Otto Vamenheis
;;;;
;;;; This program is free software: you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;;;;;;;;;;;;
;; THE JOB ;;
;;;;;;;;;;;;;

(define-values
  [list/global-job-table
   ref/global-job-table
   append!global-job-table]
  (let [[h (make-hash-table)]
        [mut (make-uni-spinlock-critical)]]
    (values
     (lambda []
       (hash-table->alist h))
     (lambda [job-name]
       (hash-get-handle h job-name))
     (lambda [job-name job-definition]
       (with-critical
        mut
        (hash-set! h job-name job-definition))))))

(define-rec job
  name
  func
  args
  future-task

  finished?
  child-structure
  status
  results

  finalizers
  actions-made
  %critical%
  )

(define get-current-job (make-parameter #f))

(define (job-finalizers-add-procedure j fin)
  (with-critical
   (job-%critical% j)
   (set-job-finalizers! j
    (cons fin (job-finalizers j)))))
(define (bump-actions-procedure j)
  (with-critical
   (job-%critical% j)
   (set-job-actions-made! j
    (1+ (job-actions-made j)))))

(define (append!job-finalizers j fin)
  (job-finalizers-add-procedure j fin))
(define (bump-actions)
  (bump-actions-procedure (get-current-job)))

(define (async-full name func args func-quoted args-quoted)

  (define (finally structure cb-status . cb-results)
    (unless (eq? 'ok cb-status)
      (tree-future-cancel
       (tree-future-task-child-index
        (job-future-task j)) ;; canceling self even though we are evaluated - because children will be cancelled anyway
       'down))

    (set-job-child-structure! j structure)
    (set-job-results! j cb-results)
    (set-job-status! j cb-status)
    (for-each
     (lambda (fin) (fin j))
     (job-finalizers j)))

  (define (callback structure cb-status . cb-results)
    (set-job-finished?! j #t))

  (define j #f)

  (define (thunk)
    (sleep-until j)
    (parameterize ((get-current-job j))
      (apply func args)))

  (define (get-job) j)

  (define future-task
    (tree-future-run-task-thunk
     thunk
     finally
     callback
     get-job))

  (set! j (job
           name
           func
           args
           future-task

           #f
           #f
           #f
           #f

           null
           0
           (dynamic-thread-critical-make)
           ))

  j)

(define-syntax-rule (async (name . args) func)
  (async-full name func args (quote func) (quote args)))

(define-syntax-rule (job-lambda#base name args argvs . bodies)
  (async-full
   (quote name)
   (lambda args . bodies)
   argvs
   (quote (lambda args . bodies))
   (quote args)))

(define-syntax-rule (job-lambda (name . args) . bodies)
  (lambda argvs
    (apply
     (lambda args
       (job-lambda#base name args argvs . bodies))
     argvs)))

(define-syntax-rule (bind-job-lambda (name . args) proc*)
  (define-values [name]
    (let ((procedure proc*))
      (append!global-job-table
       (symbol->string (quote name))
       (cons procedure (quote args)))
      (values procedure))))

(define-syntax-rule (define-job (name . args) . bodies)
  (bind-job-lambda
   (name . args)
   (job-lambda (name . args) . bodies)))

(define get-null-job
  (memconst
   ((job-lambda
     (null-job)
     (values)))))

(define (job-on-child-finish/none j)
  'whatever)
(define (job-on-child-finish/throw j)
  (let* ((child (job-child-structure j))
         (status (job-status j))
         (results (job-results j)))
    (case status
      ((ok) 0)
      ((error cancel) (throw 'job-child-failed child status results))
      (else (throw 'job-child-unknown-status child status results)))))

(define job-on-child-finish-p
  (make-parameter job-on-child-finish/none))
(define (job-on-child-finish . args)
  (apply (job-on-child-finish-p) args))

(define (job-general-wait j type)
  (if (job-finished? j)
      (begin
        (job-on-child-finish j)
        #t)
      (let* ((task (job-future-task j))
             (touch (tree-future-task-touch-procedure task)))
        (touch type 'children)
        (if (job-finished? j)
            (begin
              (job-on-child-finish j)
              #t)
            #f))))

(define (wait j)
  (job-general-wait j 'no-throw))

(define (wait-all . jobs)
  (define (wait-f j)
    (job-general-wait j 'check))

  (sleep-until (and-map wait-f jobs))
  #t)

(define (finish-please/job j . args)
  (apply
   tree-future-cancel
   (cons* (tree-future-task-child-index (job-future-task j))
          'down
          args)))

(define (actions-made-recursive/job job)
  (letin
   (do (wait job))
   (results (job-results job))
   (structure (job-child-structure job))
   (children (tree-future-children-list structure))
   (children-jobs (map (lambda (child)
                         (((tree-future-context child))))
                       children))

   (+ (job-actions-made job)
      (apply + (map actions-made-recursive/job children-jobs)))))

(define (did-something-1? job)
  (letin
   ;; (do (debug "trying to apply to (job? ~a) (list? ~a) ~a\n" (job? job) (list? job) job))
   (do (wait job))
   [re (> (actions-made-recursive/job job) 0)]
   ;; [re #f]
   ;; (do (debug "got re = ~a\n" re))
   re))

(define (did-something? . jobs)
  (apply wait-all jobs)
  (or-map (lambda (job)
            (< 0 (actions-made-recursive/job job)))
          jobs))

(define (depend . jobs)
  (parameterize
      ((job-on-child-finish-p job-on-child-finish/throw))
    (apply did-something? jobs)))

;;;;;;;;;;;;;
;; HELPERS ;;
;;;;;;;;;;;;;


(define [make-target lst]
  (cons (~a lst) lst))

(define [id:target target]
  (car target))

(define [body:target target]
  (cdr target))

(define PWD (getcwd))
(define [set!PWD new]
  (chdir new)
  (set! PWD (getcwd)))

(define [normalize-path path]
  (simplify-posix-path
   (if (absolute-file-name? path)
       path
       (append-posix-path PWD path))))

(define gumak-executable-path
  (normalize-path (get-current-program-path)))

(define [make-file-target path]
  " Path does not have to be absolute, but it will be normalized "
  (make-target
   (list 'file (normalize-path path))))

(define [make-dependent-file-target target-path . dependency-paths]
  " Same as `make-file-target',
    but this one yields #t when either of dependencies was modified
  "
  (make-target
   (list 'dependent-file
         (normalize-path target-path)
         (map normalize-path dependency-paths))))

(define [target-satisfied? target]
  (or (target-cached? target)
      (match (body:target target)
        [`(file ,path)
         (file-or-directory-exists? path)]
        [`(dependent-file ,target-path ,dependency-paths)
         (and (file-or-directory-exists? target-path)
              (let [[target-time (file-mtime target-path)]]
                (and-map (lambda [d-time] (>= target-time d-time))
                         (map file-mtime dependency-paths))))]
        [`(custom ,predicate ,cache! ,cached?)
         (predicate)]
        [els
         (throw
          'check-target-exists-error
          `(args: ,target)
          '(unsupported type of target))])))

(define-syntax-rule [with-target-lock target . bodies]
  (call-with-finally
   (lambda _
     (universal-lockr! (id:target target))
     (begin . bodies))
   (lambda _
     (universal-unlockr! (id:target target)))))

(define-syntax-rule [define-target-job [name . args] target-body . bodies]
  (bind-job-lambda
   (name . args)
   (lambda argv
     (apply
      (lambda args
        (let* ((target target-body)
               (job (job-lambda#base
                     name args argv
                     (with-target-lock
                      target
                      (unless (target-satisfied? target)
                        (begin . bodies)
                        (cache-target! target)
                        (bump-actions))))))
          (if (target-cached? target)
              (get-null-job)
              job)))
      argv))))

;;;;;;;;;;;;;;;;;;;;;
;; GOAL RESOLUTION ;;
;;;;;;;;;;;;;;;;;;;;;

(gfunc/define goal-resolver) ;; or simply "rule"

(define-syntax-rule (define-lambda-rule bind target-predicate result)
  (gfunc/instance goal-resolver
                  ((lambda (bind) target-predicate))
                  (lambda (bind) result)))

(define (need . args)
  (apply depend (map goal-resolver args)))

;;;;;;;;;;;;;;;;;;;;
;; SCRIPT RELATED ;;
;;;;;;;;;;;;;;;;;;;;

(define-rec jobs-file
  filepath
  reeval-enabled?
  default-job
  cache-table
  main
  input-args
  default-env-table)

(define current-jobs-file (make-parameter #f))

(define global-default-env-table (make-hash-table))

(define-syntax-rule (%define-env-helper name default converter)
  (let* ((jobs-file (current-jobs-file))
         (env-table (if jobs-file
                        (jobs-file-default-env-table jobs-file)
                        global-default-env-table))
         (name-q (quote name))
         (name-s (symbol->string name-q))
         (env (getenv name-s))
         (param (if converter
                    (lazy-parameter (or env default) converter)
                    (lazy-parameter (or env default))))
         (info (cons param env)))
    (hash-set! env-table name-s info)
    param))

(define-syntax define-env
  (syntax-rules ()
    ((_ name default)
     (define-env name default #f))
    ((_ name default converter)
     (define name (%define-env-helper name default converter)))))

(define commit-env-definitions
  (case-lambda
    [[] (commit-env-definitions (current-jobs-file))]
    [[jobs-file]
     (hash-table-foreach
      (jobs-file-default-env-table jobs-file)
      (lambda (name value)
        (let ((result (car value))
              (env (cdr value)))
          (setenv name (~a (result))))))]))

(define reset-env-definitions
  (case-lambda
    [[] (reset-env-definitions (current-jobs-file))]
    [[jobs-file]
     (hash-table-foreach
      (jobs-file-default-env-table jobs-file)
      (lambda (name value)
        (let ((result (car value))
              (env (cdr value)))
          (setenv name env))))]))

(define (cache-target! target)
  (let* ((job-file (current-jobs-file))
         (cache (jobs-file-cache-table job-file)))
    (match (body:target target)
      (`(file ,path)
       (hash-set! cache path #t))
      (`(dependent-file ,target-path ,dependency-paths)
       (hash-set! cache target-path #t))
      (`(custom ,predicate ,cache! ,cached?)
       (cache! target cache))
      (else
       (throw
        'cache-target-error
        `(args: ,target)
        '(unsupported type of target))))))

(define (target-cached? target)
  (let* ((job-file (current-jobs-file))
         (cache (jobs-file-cache-table job-file)))
    (match (body:target target)
      (`(file ,path)
       (hash-ref cache path #f))
      (`(dependent-file ,target-path ,dependency-paths)
       (hash-ref cache target-path #f))
      (`(custom ,predicate ,cache! ,cached?)
       (cached? target cache))
      (else
       (throw
        'cache-target-error
        `(args: ,target)
        '(unsupported type of target))))))

(define-eval-namespace eval-ns)

(define-values
    [reeval-jobs-file
     add!reeval-hooks]
  (let [[hooks (list)]
        [mut (dynamic-thread-critical-make)]]
    (values
     (lambda []
       (let* [[jobs-file (current-jobs-file)]
              [filepath (jobs-file-filepath jobs-file)]]
         (when filepath
           (load-file-in-namespace (jobs-file-filepath jobs-file)
                                   eval-ns)
           (for-each (lambda [hook] (hook)) hooks))))
     (lambda [hook]
       (with-critical
        mut
        (set! hooks (cons hook hooks)))))))

(define set-jobs-file-reeval-flag!
  (let [[mut (dynamic-thread-critical-make)]]
    (lambda [new]
      (let [[jobs-file (current-jobs-file)]]
        (with-critical
         mut
         (set-jobs-file-reeval-enabled?!
          jobs-file
          new))))))

(define [reeval-jobs-file-loop]
  (let* [[jobs-file (current-jobs-file)]
         [get-mtime (lambda []
                      (file-mtime (jobs-file-filepath jobs-file)))]
         [last-mtime (get-mtime)]]
    (let lp []
      ;; (dprintln "looping")
      (when (jobs-file-reeval-enabled? jobs-file)
        (dynamic-thread-sleep (normal->micro@unit 1/5))
        (let [[new-time (get-mtime)]]
          (when (> new-time last-mtime)
            (set! last-mtime new-time)
            (dprintln "reeval happened")
            (reeval-jobs-file))
          (lp))))))

(define [list-defined-jobs]
  (for-each
   (fn job (dprintln "~a ~a" (car job) (cdr (cdr job))))
   (list/global-job-table)))

(define [print-help-message]
  (dprintln
"
usage: gumak ::= --help | job-name arguments
       arguments ::= argument | argument arguments
"))

(define [run-job-by-name job-name arguments]
  (let [[entry (ref/global-job-table job-name)]]
    (if entry
        (depend (apply (cadr entry)
                       (map
                        (lambda [a] (eval-string-in-namespace a eval-ns))
                        arguments)))

        (throw 'invalid-job-name
               `(args: ,job-name ,arguments)
               `(tried to run job that does not exist)
               `(list of existing jobs:
                      ,(list/global-job-table))))))

(define drop-input-arg!
  (case-lambda
    [[jobs-file]
     (set-jobs-file-input-args!
      jobs-file
      (cdr (jobs-file-input-args jobs-file)))]
    [[]
     (drop-input-arg! (current-jobs-file))]))

(define-job [--help]
  (list-defined-jobs)
  (print-help-message))

(define [run-default-job jobs-file]
  (let* [[j (jobs-file-default-job jobs-file)]]
    (wait (j))))

(define consume-input-arg
  (case-lambda
    [[jobs-file]
     (let [[args (jobs-file-input-args jobs-file)]]
       (if (null? args)
           (run-default-job jobs-file)
           (run-job-by-name
            (car args)
            (cdr args))))]
    [[]
     (consume-input-arg (current-jobs-file))]))

;; `back' is a procedure that "finishes" evaluation of script
;; i.e. it does call `consume-input-arg' and `reeval-jobs-file-loop'
(define [default-main back] (back))

(define [set!gumak-default procedure]
  " Set default job. Default is --help job"
  (let [[cur (current-jobs-file)]]
    (set-jobs-file-default-job! cur procedure)))

(define [set!gumak-main procedure]
  " Set point of where script starts
  "
  (let [[cur (current-jobs-file)]]
    (set-jobs-file-main! cur procedure)))

(define (gumak-make-errors-pretty errors)
  (define (pretty err)
    (match err
      (`(job-child-failed ,child ,status ,results)
       (map pretty results))
      (else
       err)))

  (define (flaten err)
    (if (list? err)
        (cond
         ((null? err) err)
         ((null? (cdr err))
          (flaten (car err)))
         (else
          (map flaten err)))
        err))

  (define maped (flaten (map pretty errors)))
  (define s
    (with-output-to-string
      (pretty-print maped)))

  s)

(define [gumak-exe jobs-file]
  (define (finalize structure status . results)
    (parameterize [[current-jobs-file jobs-file]]
      (set-jobs-file-reeval-flag! #f)
      (parameterize ((current-output-port (current-error-port)))
        (case status
          ((ok) 0)
          ((error)
           (dprintln "gumak main failed with: \n~A" (gumak-make-errors-pretty results)))
          ((cancelled)
           (dprintln "gumak main was cancelled with: \n~A" (gumak-make-errors-pretty results)))
          (else
           (dprintln "gumak main unknown status ~A with results: \n~A" status results))))))

  (define (start)
    (parameterize [[current-jobs-file jobs-file]]
      ((jobs-file-main jobs-file)
       (lambda [] (consume-input-arg jobs-file)))))

  (with-new-tree-future-env
   (tree-future-run
    start
    finalize
    #f
    #f)))

(define [gumak-exe-script args]
  (let* [[do (or (and (list? args) (not (null? args)))
                 (throw 'expected-target-argument))]
         [target (first args)]
         [target-full (normalize-path target)]
         [env-table (hash-copy global-default-env-table)]
         [jobs-file
          (jobs-file target-full
                     #t
                     --help
                     (make-hash-table)
                     default-main
                     (cdr args)
                     env-table)]]
    (parameterize [[current-jobs-file jobs-file]]
      (set!PWD (path-parent-directory target-full))
      (reeval-jobs-file)
      (dynamic-thread-spawn reeval-jobs-file-loop)
      (commit-env-definitions jobs-file)
      (gumak-exe jobs-file))))

