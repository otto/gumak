
;;;;;;;;;;;;;
;; GENERAL ;;
;;;;;;;;;;;;;

(define-syntax-rule (define-const-rule target job-name)
  (define-lambda-rule tgt
    (equal? tgt target)
    (job-name)))

(define-syntax %help-define-file-job
  (syntax-rules ()
    ((_ (target) (name . args) . bodies)
     (begin
       (define real-target (target))
       (%help-define-file-job real-target
                              (name . args)
                              . bodies)))
    ((_ (target . dependencies) (name . args) . bodies)
     (define-target-job (name . args)
       (apply make-dependent-file-target
              (list target . dependencies))
       . bodies))
    ((_ filename (name . args) . bodies)
     (define-target-job (name . args)
       (make-file-target filename)
       . bodies))))

;; NOTE: assumes that job with no input arguments always
;; returns produces the same target!
(define-syntax define-file-job
  (syntax-rules ()
    ((_ target (name) . bodies)
     (begin
       (%help-define-file-job target (name) . bodies)
       (define-const-rule target name)))
    ((_ (target . dependencies) (name) . bodies)
     (begin
       (%help-define-file-job (target . dependencies) (name) . bodies)
       (define-const-rule target name)))
    ((_ lst (name . args) . bodies)
     (%help-define-file-job lst (name . args) . bodies))))

(define-env PREFIX #f)
(define-env BUILD_PATH_PREFIX #f)

(define-env LIMIT_PARALLEL_CMD +inf.0)

(define cmd
  (let ((critical (make-uni-spinlock-critical))
        (counter 0))
    (lambda (args)
      (let ((max (LIMIT_PARALLEL_CMD)))
        (sleep-until (< counter max))
        (critical (lambda () (set! counter (1+ counter)))) ;; a little race if fine here :)
        (call-with-finally
         (lambda _ (sh args))
         (lambda _
           (critical
            (lambda ()
              (set! counter (1- counter))))))))))

(define-file-job dir [mkdir-job dir]
  (unless (string=? "/" dir)
    (depend (mkdir-job (path-parent-directory dir))))
  (make-directory dir))

(define-env SYMLINK "ln -s")

(define [default-symlink-cmd target-name source-name]
  (stringf "~a ~a -c -o ~a ~a"
           (SYMLINK)
           source-name
           target-name))

(define SYMLINK_CMD (make-parameter default-symlink-cmd))

(define-file-job target [symlink-file target source]
  (depend (mkdir-job (path-parent-directory target)))
  (cmd ((SYMLINK_CMD) target source)))

(define-job (echo . exprs)
  (for-each
   (lambda (expr)
     (display expr)
     (newline))
   exprs))

;;;;;;;;;;;;;;;;
;; C-SPECIFIC ;;
;;;;;;;;;;;;;;;;

(define-env CC "gcc")
(define-env CFLAGS "")

(define [default-compile-cmd target-name source-name]
  (stringf "~a ~a -c -o ~a ~a"
           (CC)
           (CFLAGS)
           target-name
           source-name))

(define COMPILE_CMD (make-parameter default-compile-cmd))

(define [default-get-c-target-name source-name]
  (let [[prefix (BUILD_PATH_PREFIX)]
        [suffix (string-append source-name ".o")]]
    (if prefix
        (path-redirect prefix suffix)
        suffix)))

(define GET_C_OBJ_NAME
  (make-parameter default-get-c-target-name))

(define-env LINK "gcc")
(define-env LDFLAGS "")

(define [default-link-cmd target-name objs]
  (stringf "~a -o ~a ~a ~a"
           (LINK)
           target-name
           (string-join objs " ")
           (LDFLAGS)))

(define LINK_CMD (make-parameter default-link-cmd))

(define-file-job (((GET_C_OBJ_NAME) src) src) [compile-c-source src]
  (depend (mkdir-job (path-parent-directory ((GET_C_OBJ_NAME) src))))
  (cmd ((COMPILE_CMD)
        ((GET_C_OBJ_NAME) src)
        src)))

(define-target-job [link-c-objs target-name objs]
  (apply make-dependent-file-target
         (cons* target-name
                objs))
  (cmd ((LINK_CMD)
        target-name
        objs)))

(define-lambda-rule file
  (string-suffix? ".o" file)
  (let* ((build-prefix (BUILD_PATH_PREFIX))
         (unprefixed (if build-prefix
                         (string-drop file (string-length build-prefix))
                         file))
         (original (path-without-extension unprefixed)))
    (compile-c-source original)))

