# Gumak

gumak is a build system with procedural makefiles written in scheme.

Gumak makefiles are procedural by default, which is intentional,
since that, hopefully, gives more control and flexibilty for the user.
They are also more composable because gumak basic unit of composition
is `job` (which is just an asynchronous procedure), while Makefile's
basic unit is `rule`. Rules don't have inputs or outputs.
gumak uses rules as an optional "glue" between jobs.

There is also `ramak` version of Gumak, which uses Racket
as its makefiles interpreter while gumak uses Guile.
Other than that, they are pretty much identical.

# Usage

Having a gumakscript, like in example below:

```lisp
(define-job (all)
    (depend (compile-c-source "a.c")
            (compile-c-source "b.c")
            (compile-c-source "c.c"))
    (depend (link-c-objs "a.out"
                         (list
                            "a.c.o"
                            "b.c.o"
                            "c.c.o")))
    (printfln "all done"))
```

Run

    ./gumak example.gumak.scm all

Or

    ./gumak --racket example.gumak.scm all

Functions `compile-c-source` and `link-c-objs` are already defined,
for convinience, but there is nothing magical in them.

# More examples

```lisp
(define-job (all)
    ;; instead of compiling directly,
    ;; we can ask someone else to compile for us.
    ;; Gumak adds default compile rule that uses `compile-c-source`
    ;; for "*.o" objects
    (need "a.c.o" "b.c.o" "c.c.o")

    (depend (link-c-objs "a.out"
                         (list
                            "a.c.o"
                            "b.c.o"
                            "c.c.o")))
    (printfln "all done"))
```

```lisp
;; We can overwrite default compile rule:
(define-lambda-rule file
    (string-suffix? ".o" file)
    (begin
      (printfln "compiling file: ~a" file)
      (compile-c-source
       (path-withouth-extension file))))

(define-job (all)
    (need "a.c.o" "b.c.o" "c.c.o")

    (depend (link-c-objs "a.out"
                         (list
                            "a.c.o"
                            "b.c.o"
                            "c.c.o")))
    (printfln "all done"))
```


```lisp
;; Or if prefer to do everything ourselves
(define-job (my-compile file)
    (sh (stringf "gcc -o '~a' '~a'"
          file (path-withouth-extension file))))

(define-lambda-rule file
    (string-suffix? ".o" file)
    (my-compile file))

(define-job (all)
    (need "a.c.o" "b.c.o" "c.c.o")

    (depend (link-c-objs "a.out"
                         (list
                            "a.c.o"
                            "b.c.o"
                            "c.c.o")))
    (printfln "all done"))
```
