
all:
	sh build.sh

install: all
	ln -sf $(PWD)/gumak $(HOME)/.local/bin/

reinstall: | clean install

clean:
	git clean -dfx
