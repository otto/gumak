#! /bin/sh

stat euphrates/Makefile || git submodule update --init

(cd euphrates/ && make) &

cat src/guile.header.scm src/core.scm src/common.scm src/guile.footer.scm > gumak.scm
chmod +x gumak.scm

cat src/racket.header.rkt src/core.scm src/common.scm src/racket.footer.scm > ramak.rkt
chmod +x ramak.rkt

wait

