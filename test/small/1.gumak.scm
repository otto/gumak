
(define-job (test1)
  (letin
   (child1 (async "child"
                  (lambda (x)
                    (dprintln "in child 1")
                    (* x 5))
                  (list 3)))
   (child2 (async "child2"
                  (lambda (x)
                    (dprintln "in child 2")
                    (* x 6))
                  (list 3)))
   (do (depend child1 child2))
   (do (dprintln "done 1"))
   0))

(define-job (test2)

  (define-job (test2-job x)
    (dprintln "test2-job got x = ~a" x))

  (define-job (test2-calc-job x)
    (* x 100))

  (letin
   (child1 (test2-job 777))
   (child2 (test2-calc-job 222))
   (do (depend child1 child2))
   (do (dprintln "done 2"))
   0))

(define-job (test3)

  (define-job (test2-job x)
    (dprintln "test2-job got x = ~a" x))

  (define-job (test2-calc-job x)
    (bump-actions)
    (* x 100))

  (letin
   (child1 (test2-job 777))
   (child2 (test2-calc-job 222))
   (begin
     (depend child1 child2)
     (dprintln "actions made 1: ~a" (actions-made-recursive/job child1))
     (dprintln "actions made 2: ~a" (actions-made-recursive/job child2))
     (assert-equal 0 (actions-made-recursive/job child1))
     (assert-equal 1 (actions-made-recursive/job child2))
     (assert-equal 2 (actions-made-recursive/job child2))
     (dprintln "done 2"))))

(define [run-gumak cmd]
  (sh (string-append gumak-executable-path " " cmd)))

(define-job [all]
  (run-gumak "1.gumak.scm test1")
  (run-gumak "1.gumak.scm test2")
  (run-gumak "1.gumak.scm test3")
  )

(define (main back)
  (dprintln "hello")
  (back)
  )

(set!gumak-main main)

