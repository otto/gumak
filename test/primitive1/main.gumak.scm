
(define-job [print-sleep x]
  (printf "2 + x = ~a\n" (+ 2 x))
  (dynamic-thread-sleep (normal->micro@unit 1))
  (dprintln "slept!")
  (bump-actions))

(define-job [hell x]
  (printf "emmmmmm\n")
  ;; (async (printf "2 + x = ~a\n" (+ 2 x)))
  ;; (depend (printf "2 + x = ~a\n" (+ 2 x)))
  (if (did-something?
       ;; (async
       ;;  (fn x
       ;;      (begin
       ;;        (printf "2 + x = ~a\n" (+ 2 x))
       ;;        (sleep 2)
       ;;        (bump-actions)))
       ;;  712))
       (print-sleep x))
      (printf "yes, did something\n")
      (printf "no, didnt do anything\n"))
  ;; (printf "2 = ~a\n" 2)
  (dprintln "2 = ~a\n" 2)
  ;; (return 55)
  (+ 2 x))

(define-job [manager-job]
  (letin
   (do (dprintln "started manager\n"))
   [cur (get-current-job)]
   [loop-th #f]
   [fin (lambda [job reason]
          (dprintln "running fin!\n")
          (when (and reason loop-th)
              (sys-thread-cancel loop-th)))]
   (do (append!job-finalizers cur fin))
   (do (set! loop-th
         (sys-thread-spawn
          (defloop []
            ;; (printf "got line = ~a\n" (get-line (current-input-port)))
            (usleep 10)
            ;; (get-line (current-input-port))
            (loop)))))
   (apploop [] []
            (dynamic-thread-sleep 100000)
            (when (and loop-th (not (sys-thread-exited? loop-th)))
              (loop)))))

(define-job [start]
  (dprintln "IM HERE")
  (letin
   [h (hell 2)]
   [mng (manager-job)]
   (do (dprintln "waiting for h"))
   (do (wait h))
   (do (dprintln "killing mng"))
   (do (finish-please/job mng 'done))
   0))

(set!gumak-default start)

