
(set!PWD "nothing")

(define-env CFLAGS "-Isrc -I/usr/include/SDL2")
(define-env BUILD_PATH_PREFIX "build/")
(define-env LDFLAGS "-lSDL2 -Wl,--no-undefined -lm -ldl -lasound -lm -ldl -lpthread -lpulse-simple -lpulse -lsndio -lX11 -lXext -lXcursor -lXinerama -lXi -lXrandr -lXss -lXxf86vm -lwayland-egl -lwayland-client -lwayland-cursor -lxkbcommon -lpthread -lrt")
(define-env CSRCS
  (filter (fn file (string-suffix? ".c" file))
          (map car (directory-files-rec "src"))))
(define-env OBJS (map (GET_C_OBJ_NAME) (CSRCS)))

(define-lambda-rule file
  (equal? "build/main.exe" file)
  (cstart))

(define-job (user-need x)
  (need x))

(define-job [cstart]
    (dprintln "in cstart")

    (depend (init-submodule))

    (if (apply need (OBJS))
        (dprintln "compiled")
        (dprintln "didnt need to compile"))

    (if (depend (link-c-objs "build/main.exe" (OBJS)))
        (dprintln "linked")
        (dprintln "didnt need to link"))

    (dprintln "bye")
    )

(define-job [with-tcc . args]
  (with-dynamic
   [[CC "tcc"]
    [LINK "tcc"]
    [CFLAGS (string-append (CFLAGS)
                           " -DSDL_DISABLE_IMMINTRIN_H")]]
   (drop-input-arg!)
   (consume-input-arg)))

(define-job [with-parameter key value . args]
  (with-dynamic
   [[key value]]
   (drop-input-arg!)
   (drop-input-arg!)
   (drop-input-arg!)
   (consume-input-arg)))

(define-job [init-submodule]
  (sh "git submodule update --init"))

(define-job [say-hello to]
  (dprintln "Hello, ~a!" to))

(define-job [clean]
  (sh "git clean -dfx"))

