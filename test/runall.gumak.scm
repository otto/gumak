
(set!PWD "..")

(define [run-gumak cmd]
  (sh (string-append gumak-executable-path " " cmd)))

;; TODO: fix why its not printing as it goes?
;;       Only gumak.scm not updating console continously =/
(define-job [all]
  (setenv "LIMIT_PARALLEL_CMD" "4")

  (reset-env-definitions) ;; unset CFLAGS and such

  ;; (sh "while true; do echo kek; sleep 1 ; done")
  (run-gumak "test/primitive1/main.gumak.scm")

  (run-gumak "test/simplec/main.gumak.scm clean")
  (run-gumak "test/simplec/main.gumak.scm cstart")
  (run-gumak "test/simplec/main2.gumak.scm clean")
  (run-gumak "test/simplec/main2.gumak.scm cstart")
  (run-gumak "test/simplec/main3.gumak.scm clean")
  (run-gumak "test/simplec/main3.gumak.scm cstart")
  (run-gumak "test/simplec/main4.gumak.scm all")

  (run-gumak "test/hardc/make.gumak.scm clean")
  (run-gumak "test/hardc/make.gumak.scm cstart")

  (run-gumak "test/hardc/make.gumak.scm clean")
  (run-gumak "test/hardc/make.gumak.scm with-parameter CC \\\"clang\\\" with-parameter LINK \\\"clang\\\" cstart")
  )

(set!gumak-default all)

