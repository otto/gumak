
(define [on-update]
  (dprintln "updated!"))

(define-job [say-hello to]
  (dprintln "Hello there, ~a!" to))

(define-job [listen]
  (let lp []
    (dprintln "HAHA")
    (dynamic-thread-sleep (normal->micro@unit 1/2))
    (depend (say-hello "listener"))
    (lp)))

(define [main back]
  (add!reeval-hooks on-update)
  (back))

(set!gumak-main main)

