
(define-job [clean]
  (sh "git clean -dfx"))

(define-job [cstart]
  (dprintln "in cstart")
  (if (depend
       (compile-c-source "a.c")
       (compile-c-source "b.c")
       (compile-c-source "c.c")
       )
      (begin
        (if (depend
             (link-c-objs "main.exe"
                          (list "a.c.o"
                                "b.c.o"
                                "c.c.o")))
            (dprintln "linked")
            (dprintln "didnt need to link")))
      (dprintln "didnt need to compile"))
  (dprintln "bye")
  )
