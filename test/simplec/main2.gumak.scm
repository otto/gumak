
(define-job [clean]
  (sh "git clean -dfx"))

(define-job [cstart]
  (need "a.c.o" "b.c.o" "c.c.o")
  (dprintln "linking")
  (link-c-objs "main.exe"
               (list "a.c.o"
                     "b.c.o"
                     "c.c.o"))
  (dprintln "bye")
  )
