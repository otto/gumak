
(define-job [clean]
  (sh "git clean -dfx"))

(define compile-counter 0)

(define-target-job (my-compile src)
  (make-dependent-file-target ((GET_C_OBJ_NAME) src) src)
  (set! compile-counter (1+ compile-counter))
  (sh ((COMPILE_CMD)
       ((GET_C_OBJ_NAME) src)
       src)))

(define-job [cstart]
  (depend (clean))

  (apply depend (map my-compile (list "a.c" "b.c" "c.c")))
  (apply depend (map my-compile (list "a.c" "b.c" "c.c")))

  (link-c-objs "main.exe"
               (list "a.c.o"
                     "b.c.o"
                     "c.c.o"))

  (assert-equal compile-counter 3)
  )
