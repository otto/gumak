
(define-job [clean]
  (sh "git clean -dfx"))

(define-file-job "main.exe" (make-main)
  (need "a.c.o" "b.c.o" "c.c.o")
  (link-c-objs "main.exe"
               (list "a.c.o"
                     "b.c.o"
                     "c.c.o")))

(define-job [all]
  (depend (clean))

  (need "main.exe")
  (assert (target-satisfied? (make-file-target "main.exe")))

  (dprintln "bye")
  )
